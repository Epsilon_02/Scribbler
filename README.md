
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)](https://mit-license.org/)
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Scribbler

This project is massively inspired by [Scribe](https://sr.ht/~edwardloveall/Scribe/)

Style from [water.css](https://github.com/kognise/water.css)

An alternative frontend of `dev.to` written in [SvelteKit](https://kit.svelte.dev/)

## Development

- Clone this project
- run `bun install` (install [bun](https://bun.sh/) if not already installed)
- run `bun --bun run dev`
- start coding

## Build and run docker container

```bash
docker build --tag scribbler .
docker run -p 3000:3000 scribbler
```

> Currently `docker compose` seems not working. Working on a fix.
